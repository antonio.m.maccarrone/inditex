// ***********************************************
// This example commands.js shows you how to
// create various custom commands and overwrite
// existing commands.
//
// For more comprehensive examples of custom
// commands please read more here:
// https://on.cypress.io/custom-commands
// ***********************************************
//
//
// -- This is a parent command --
// Cypress.Commands.add('login', (email, password) => { ... })
//
//
// -- This is a child command --
// Cypress.Commands.add('drag', { prevSubject: 'element'}, (subject, options) => { ... })
//
//
// -- This is a dual command --
// Cypress.Commands.add('dismiss', { prevSubject: 'optional'}, (subject, options) => { ... })
//
//
// -- This will overwrite an existing command --
// Cypress.Commands.overwrite('visit', (originalF, url, options) => { ... })

Cypress.Commands.add('searchAutomation', () => { 
    cy.visit('http://google.com');

    cy.wait(1000).contains('Acepto').click();
    
    cy.get('body')
    .should('be.visible')
    .find(".gLFyf")
    .type('automatización{enter}');
});

Cypress.Commands.add('selectWikipedia', () => {
    cy.contains('es.wikipedia.org').click();
});
