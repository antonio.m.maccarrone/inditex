describe('Search automation', () => {
    it('search for automatizacion in google', () => {
        cy.searchAutomation();
    });
});

describe('Select wikipedia google result', () => {
    it('look for wikipedia result', () => {
        cy.selectWikipedia();
    });
});

describe('Wikipedia assert', () => {
    it('for first automation year', () => {
        cy.searchAutomation();
        cy.selectWikipedia();

        cy.contains(Cypress.env('yearFirstAuto'));
    });
});

describe('Wikipedia snapshots', () => {
    it('take wikipedia snapshot', () => {
        cy.searchAutomation();
        cy.selectWikipedia();

        // Snapshot testing
        cy.get('body').matchImageSnapshot('capture');
    });

    it('take wikipedia fullpage screenshot', () => {
        cy.searchAutomation();
        cy.selectWikipedia();

        // Screenshot 
        cy.screenshot('wikipedia-screenshot', {capture:'fullPage'});
    });
});