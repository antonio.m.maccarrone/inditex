const axios = require('axios');
const url = "https://petstore.swagger.io/v2";
const PET_STATUS = "sold";

async function createUser(user) {
	try {
		return await axios.post(`${url}/user`, user);
	} catch (err) {
		console.error(err);
	}
}

async function getUser(userName) {
	try {
		return await axios.get(`${url}/user/${userName}`);
	} catch (err) {
		console.error(err);
	}
}

async function handleUserCreation() {
	const rand = Math.floor((Math.random() * 10000) + 1);

	const mockUser = {
		username: "Test" + rand,
		firstName: "Name" + rand,
		lastName: "LastName",
		email: "email@mail.com",
		password: rand,
		userStatus: 1
	};

	createUser(mockUser).then((res) => {
		if (res && res.data && res.data.code !== 200) {
			throw Error("Fail on create user");
		}

		getUser(mockUser.username).then((user) => console.dir(user.data, { 'maxArrayLength': null }));
	});
}

async function getPets(status) {
	try {
		return await axios.get(`${url}/pet/findByStatus?status=${status}`);
	} catch (err) {
		console.error(err);
	}
}

async function handlePetOrder() {
	let orderedPets = {};

	return await getPets(PET_STATUS)
		.then(res => res.data.map(e => ({ "id": e.id, "name": e.name })))
		.then(res => res.map(e => orderedPets[e.name] = (orderedPets[e.name] || 0) + 1))
		.then(() => console.dir(orderedPets, { 'maxArrayLength': null }));
};

async function main() {
	handleUserCreation();

	handlePetOrder();
}

main();