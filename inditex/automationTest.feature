Feature: Automation Search

    Description: The user makes a search and goes to the first Wikipedia result. 
    After finding some data the user take a page screenshot.

Scenario: User search "Automatización" on google
    Given the user is on "http:google.com"
    When the user searches for "Automatización" 
    Then the system retrieve results

Scenario: User goes to the first Wikipedia result from google
    Given the user is on "http:google.com"
    And the user searches for "Automatización"
    When the user clicks on the wikipedia results title
    Then the page redirects the user to "https://es.wikipedia.org/wiki/Automatizaci%C3%B3n_industrial"  

Scenario: User check automation first process year
    Given the user is on "https://es.wikipedia.org/wiki/Automatizaci%C3%B3n_industrial"
    When the user looks for the first automation 
    Then "yearFirstAuto" = 1801

Scenario: User take a Wikipedia's page screenshot
    Given the user is on "https://es.wikipedia.org/wiki/Automatizaci%C3%B3n_industrial"
    When the page is fully loaded
    Then the system takes a fullpage screenshot
    And save it on /cypress/screenshots
