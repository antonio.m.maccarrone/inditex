Tarea 1 - Prueba exploratoria y reporte de bugs:

La primera tarea es encontrar un bug, nos da igual en que web de internet lo encuentres, lo importante es poder encontrar y reportarlo. No te preocupes porque internet está lleno de ellos.
 

Tarea2 – Automatización de una web:

Debes realizar una automatización consistente en:

-          Búsqueda en google de la palabra “automatización”
-          Buscar el link de la Wikipedia resultante
-          Comprobar en qué año se hizo el primer proceso automático
-          Realizar un screenshot de la página de la Wikipedia.


Tarea 3 – Tratamiento de datos en APIs:

En este enlace encontraras la documentación de la API de una tienda de mascotas: https://petstore.swagger.io/

1.       Crea tu usuario mediante petición HTTP y posteriormente  recupera sus datos llamando al servicio correspondiente.

2.       Recoge mediante petición HTTP, el JSON que retorna el endpoint /pet/findByStatus y lista mediante una función los nombres de las mascotas que se hayan vendido.
-          El formato de salida deberá estar formado por la tupla {id, name}.
-          Puedes utilizar la estructura de datos que prefieras.

3.       Crea una clase cuyo constructor requiera de la estructura de datos anterior y realiza un método que pueda recorrerla para poder identificar cuantas mascotas se llaman igual.
-          Ejemplo de salida:
{   “William”: 11, “ Floyd”: 2}



# Como output, te pediremos el código (puedes separarlo en archivos como quieras) y los resultados de salida de los puntos anteriores. Recuerda que puedes utilizar el lenguaje que prefieras y cualquier mejora adicional será bien considerada.
# La prueba puede llevarte entre 2 o 4 horas. Dispones hasta el lunes 12/04/2021 para poder enviar las respuestas, aunque en caso de tener cualquier contratiempo por favor avísanos.
