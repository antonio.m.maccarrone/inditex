summary: [ZARA] Homepage | Change Man homepage view missing image - Functionality

labels: design, FE

priority: minor

description {
url: https://www.zara.com/es/es/hombre-nuevo-l711.html?v1=1717453
env: PROD
browser: ALL

Given the user is on the Men's homepage 
When changes the view using the top right slider
Then a white image appear next to the first one

Please fix this blank space so the site has consistency  

Also, if the user uses the right filter, the blank space disappear
}

steps to reproduce:
# The user is on "https://www.zara.com/es/"
# The user clicks on Men's hero image
# The system redirect the user to https://www.zara.com/es/es/hombre-nuevo-l711.html?v1=1717453
# The user change the view using the top right slider
# The second image is blank

expected behavior: As Women's homepage no blank space should be placed

actual behavior: There is a blank space in the second image

For more detail please take a look into the images attached. 

!Zara Hombre - Missing image.png!
!Zara Hombre - Missing image 02.png!